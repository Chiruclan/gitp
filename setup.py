from distutils.core import setup
import py2exe

setup(name = "GITp",
      version = "1",
	  description = "A GIT framework for easy using with hashlist and version file",
	  author = "Pascal Vahlberg",
	  author_email = "support@chiruclan.de",
	  url = "http://www.chiruclan.de/",
	  console = ['gitp.py'])